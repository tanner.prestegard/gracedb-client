ligo-gracedb
============

.. image:: https://git.ligo.org/lscsoft/gracedb-client/badges/master/pipeline.svg
   :target: https://git.ligo.org/lscsoft/gracedb-client/commits/master/

.. image:: https://img.shields.io/pypi/v/ligo-gracedb.svg?style=popout
   :target: https://pypi.org/project/ligo-gracedb/

.. image:: https://img.shields.io/pypi/pyversions/ligo-gracedb.svg?style=popout
   :target: https://pypi.org/project/ligo-gracedb/


ligo-gracedb is a Python package which provides tools for accessing the API of the **Gra**\ vitational-wave **C**\ andidate **E**\ vent **D**\ ata\ **b**\ ase (|gracedb|_).
It is compatible with Python 2.7, 3.5, 3.6, and 3.7.

ligo-gracedb provides functionality which allows users to do the following:

- Create, update, and retrieve information about events and superevents
- Create and retrieve log entries
- Upload and download files
- Apply and remove labels to/from events and superevents
- And more!


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   quickstart
   installation
   user_guide
   cli
   api
   contributing
   troubleshooting
   changelog
