Quick start
===========

Installation
------------
With `pip`_::

    pip install ligo-gracedb


Basic usage
-----------

Instantiate a client object:

.. code-block:: python

    >>> from ligo.gracedb.rest import GraceDb
    >>> client = GraceDb()

Retrieve an event from the server:

.. code-block:: python

    >>> response = client.event('G1234')
    >>> print(response.json())
    {u'created': u'2019-01-16 19:51:29 UTC',
     u'extra_attributes': {
       u'MultiBurst': {
         u'amplitude': 5.017162,
         u'bandwidth': 256.0,
         u'central_freq': 1392.169556,
         u'confidence': None,
         u'duration': 0.023438,
         u'false_alarm_rate': None,
         u'ifos': u'H1,L1',
         u'ligo_angle': None,
         u'ligo_angle_sig': None,
         u'ligo_axis_dec': 69.422546,
         u'ligo_axis_ra': 201.224625,
         u'peak_time': None,
         u'peak_time_ns': None,
         u'single_ifo_times': u'1042312876.5073,1042312876.5090',
         u'snr': 7.28010988928052,
         u'start_time': 1042312876,
         u'start_time_ns': 500000000
       }
     },
     u'far': 0.00019265,
     u'far_is_upper_limit': False,
     u'gpstime': 1042312876.509,
     u'graceid': u'G1234',
     u'group': u'Test',
     u'instruments': u'H1,L1',
     u'labels': [u'DQV', u'INJ', u'PE_READY'],
     u'likelihood': 53.0,
     u'links': {
       u'emobservations': u'https://gracedb.ligo.org/api/events/G1234/emobservation/',
       u'files': u'https://gracedb.ligo.org/api/events/G1234/files/',
       u'labels': u'https://gracedb.ligo.org/api/events/G1234/labels/',
       u'log': u'https://gracedb.ligo.org/api/events/G1234/log/',
       u'neighbors': u'https://gracedb.ligo.org/api/events/G1234/neighbors/',
       u'self': u'https://gracedb.ligo.org/api/events/G1234',
       u'tags': u'https://gracedb.ligo.org/api/events/G1234/tag/'
     },
     u'nevents': None,
     u'offline': False,
     u'pipeline': u'CWB',
     u'submitter': u'albert.einstein@LIGO.ORG',
     u'superevent': None}


Using the command-line interface
--------------------------------
A command-line tool (``gracedb``) is provided which encapsulates most of the Python package's functionality.
To see a list of available commands::

    gracedb --help

Get an event::

    gracedb get event G1234

Search for events::

    gracedb search events 'far < 1e-6' --columns=graceid,labels,far
